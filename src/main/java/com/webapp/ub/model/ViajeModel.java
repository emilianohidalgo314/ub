package com.webapp.ub.model;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "viajes")
public class ViajeModel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "fecha_asignacion")
    private Date startDate;

    @Column(name = "destino")
    private String destino;

    @Column(name = "fecha_regreso")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "pasajero_id", referencedColumnName = "id")
    private UserModel pasajero;

    @ManyToOne
    @JoinColumn(name = "conductor_id", referencedColumnName = "id")
    private  UserModel conductor;

    public ViajeModel() {
        super();
    }


    public ViajeModel(Date startDate, String destino, Date endDate, UserModel pasajero, UserModel conductor) {
        super();
        this.startDate = startDate;
        this.destino = destino;
        this.endDate = endDate;
        this.pasajero = pasajero;
        this.conductor = conductor;
    }
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public UserModel getPasajero() {
        return pasajero;
    }

    public void setPasajero(UserModel pasajero) {
        this.pasajero = pasajero;
    }

    public UserModel getConductor() {
        return conductor;
    }

    public void setConductor(UserModel conductor) {
        this.conductor = conductor;
    }


    public void setStartDate(LocalTime now) {
    }
}
