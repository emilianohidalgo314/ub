package com.webapp.ub.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "usuarios")
public class UserModel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "username")
    String username;

    @Column(name = "nombre")
    String nombre;

    @Column(name = "apellido")
    String apellido;

    @Column(name = "pass")
    String pass;

    @Column(name = "rol")
    String rol;

    @OneToMany(mappedBy = "pasajero")
    List<ViajeModel> viajes;
    public UserModel(){
        super();
    }

    public UserModel(String username, String nombre, String apellido, String pass, String rol) {
        super();
        this.username = username;
        this.nombre = nombre;
        this.apellido = apellido;
        this.pass = pass;
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
