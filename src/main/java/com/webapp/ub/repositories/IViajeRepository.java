package com.webapp.ub.repositories;

import com.webapp.ub.model.ViajeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IViajeRepository extends JpaRepository<ViajeModel,Integer>{
    List<ViajeModel> findById(long id);
    List<ViajeModel> findByPasajero(long id);

}
