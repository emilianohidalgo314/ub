package com.webapp.ub.repositories;

import com.webapp.ub.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IUsuarioRepository extends JpaRepository<UserModel,Integer>{
    Optional<UserModel> findByUsername(String username);
    List<UserModel> findById(Long id);
    Optional<UserModel> findByUsernameAndPass(String username, String pass);
    List<UserModel> findByRol(String rol);
    void deleteById(Long idToDelete);
}
