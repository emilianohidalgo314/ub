package com.webapp.ub.controllers;

import com.webapp.ub.model.LoginModel;
import com.webapp.ub.model.UserModel;
import com.webapp.ub.model.ViajeModel;
import com.webapp.ub.services.UserService;
import com.webapp.ub.services.ViajesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Controller
public class ViajeController {
@Autowired
    UserService userService;
@Autowired
    ViajesService viajesService;

@GetMapping("/viajes/pasajero/{id}")
public ModelAndView viewAll(@PathVariable("id")
                                 Long identificador) {
    ModelAndView mav = null;
    try {
      mav   = new ModelAndView("viajesPasajero");
        mav.addObject("viajes", viajesService.findPassengerTrips(identificador));
    }catch (Exception e){
        mav = new ModelAndView("sinViajes");
        mav.addObject("usuario", userService.findById(identificador));
    }
    return mav;
}

@GetMapping("/viajes/viajar/{id}")
public ModelAndView travel(@PathVariable("id")
                                 Long id) {
    ModelAndView mav = new ModelAndView("viajar");
    mav.addObject("viaje", new ViajeModel());
    mav.addObject("uID", userService.findById(id).getId());
    return mav;
}

@PostMapping("/viajes/registrarViaje/{id}")
    public String registrarViaje(@ModelAttribute("viaje") ViajeModel viaje, @PathVariable("id")
        Long id){
    ModelAndView mav = null;
    List<UserModel> conductores = userService.findDriver();
    viaje.setStartDate(LocalTime.now());
    viaje.setPasajero(userService.findById(id));
    viaje.setConductor(conductores.get(ThreadLocalRandom.current().nextInt(1, conductores.size() )));
    viajesService.register(viaje);
    mav = new ModelAndView("home");
    mav.addObject("user", userService.findById(id));

    return "redirect:/pasajero/ver/{id}";
}

}
    