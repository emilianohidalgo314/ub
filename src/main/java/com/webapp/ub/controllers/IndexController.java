package com.webapp.ub.controllers;

import com.webapp.ub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController {
    @Autowired
    UserService userService;

    @GetMapping("/")
    public String redirect(){
        return "redirect:/home/";
    }
    @GetMapping("/home/")
    public String homePage(ModelAndView mav, @PathVariable("id") Long id) {
        mav.addObject("user",userService.findById(id));
        return "home";
    }
}


