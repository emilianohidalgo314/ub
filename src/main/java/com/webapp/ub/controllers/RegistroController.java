package com.webapp.ub.controllers;

import com.webapp.ub.model.LoginModel;
import com.webapp.ub.model.RegistroModel;
import com.webapp.ub.model.UserModel;
import com.webapp.ub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class RegistroController {
@Autowired
    UserService userService;

@GetMapping("/registro")
    public ModelAndView showRegistro(){
    ModelAndView mav = new ModelAndView("register");
    mav.addObject("usuario", new UserModel());
    return mav;
}

@PostMapping("/registro_process")
    public String validateLogin(@ModelAttribute("usuario") UserModel usuario){
     ModelAndView mav = null;
    if (null == userService.findByUsername(usuario.getUsername())) {
        userService.register(usuario);
        return "redirect:/pasajero/ver/" + usuario.getId();

    } else {

        return "redirect:/registro";
    }

}

}
