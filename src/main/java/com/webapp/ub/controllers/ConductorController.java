package com.webapp.ub.controllers;

import com.webapp.ub.model.UserModel;
import com.webapp.ub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/conductor")
public class ConductorController {
    @Autowired
    UserService userService;

    @GetMapping(path = "/ver/{id}")

    public ModelAndView verConductor(@PathVariable("id")
                                        Long identificador) {

        UserModel conductor = this.userService.findById(identificador);

        ModelAndView mav = new ModelAndView("homeConductor");
        mav.addObject("Conductor", conductor);

        return mav;

    }

    @PostMapping("/add")
    public String registrarConductor(@ModelAttribute(name = "conductorModel")
                                            UserModel userModel) {

        if (userModel.getId() == null || userModel.getId() == 0) {

            this.userService.register(userModel);

        } else {

            this.userService.edit(userModel);

        }

        System.out.println(userModel);

        return "redirect:/conductor/";

    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id")
                                     Long identificador) {

        UserModel ConductorFound =
                this.userService.findById(identificador);
        ModelAndView mav =
                new ModelAndView("conductors/newconductorForm",
                        "conductorModel",
                        ConductorFound);
        return mav;

    }

    @GetMapping(path = "/delete/{id}")
    public String delete(@PathVariable("id")
                                 Long idToDelete) {

        this.userService.delete(idToDelete);

        return "redirect:/Conductor/all";

    }



}
