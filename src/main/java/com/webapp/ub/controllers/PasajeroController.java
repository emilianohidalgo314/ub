package com.webapp.ub.controllers;

import com.webapp.ub.model.UserModel;
import com.webapp.ub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/pasajero")
public class PasajeroController {
    @Autowired
    UserService userService;

    @GetMapping(path = "/ver/{id}")

    public ModelAndView verPasajero(@PathVariable("id")
                                        Long identificador) {

        UserModel pasajero = this.userService.findById(identificador);

        ModelAndView mav = new ModelAndView("homePasajero");
        mav.addObject("usuario", pasajero);

        return mav;

    }

    @PostMapping("/add")
    public String registrarPasajero(@ModelAttribute(name = "pasajeroModel")
                                            UserModel userModel) {

        if (userModel.getId() == null || userModel.getId() == 0) {

            this.userService.register(userModel);

        } else {

            this.userService.edit(userModel);

        }

        System.out.println(userModel);

        return "redirect:/pasajero/";

    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id")
                                     Long identificador) {

        UserModel PasajeroFound =
                this.userService.findById(identificador);
        ModelAndView mav =
                new ModelAndView("pasajeros/newpasajeroForm",
                        "pasajeroModel",
                        PasajeroFound);
        return mav;

    }

    @GetMapping(path = "/delete/{id}")
    public String delete(@PathVariable("id")
                                 Long idToDelete) {

        this.userService.delete(idToDelete);

        return "redirect:/pasajero/all";

    }



}
