package com.webapp.ub.controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.webapp.ub.model.LoginModel;
import com.webapp.ub.model.UserModel;
import com.webapp.ub.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {
@Autowired
    UserService userService;

@GetMapping("/login")
    public ModelAndView showLogin(){
    ModelAndView mav = new ModelAndView("login");
    mav.addObject("login", new LoginModel());
    return mav;
}

@PostMapping("/log_validation")
    public ModelAndView validateLogin(@ModelAttribute("login") LoginModel login){
     ModelAndView mav = null;
    UserModel user = userService.validate(login);

    if (null != user) {

        mav = new ModelAndView("home");

        mav.addObject("user", user);

    } else {
        mav = new ModelAndView("login");
        mav.addObject("message", "Username or Password is wrong!!");

        }
    return mav;
    }

}
