package com.webapp.ub.services;

import com.webapp.ub.model.LoginModel;
import com.webapp.ub.model.UserModel;
import com.webapp.ub.repositories.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    IUsuarioRepository repository;

    public UserModel register(UserModel newPasajero) {

        return this.repository.save(newPasajero);

    }

    public boolean isPassenger(UserModel user){
        return user.getRol().equals("PASAJERO");
    }

    public List<UserModel> findDriver() {
        return this.repository.findByRol("CONDUCTOR");
    }

    public List<UserModel> findPassengers() {
        return this.repository.findByRol("PASAJERO");
    }

    public UserModel findById(Long id) {

        List<UserModel> found = this.repository.findById(id);

        try {
            return found.get(0);
        } catch (NoSuchElementException e) {
            System.out.println("No se encontró el elemento");
        }

        return null;
    }
    public UserModel findByUsername(String username) {

        Optional<UserModel> found = this.repository.findByUsername(username);

        try {
            return found.get();
        } catch (NoSuchElementException e) {
            System.out.println("No se encontró el elemento");
        }

        return null;
    }

    public UserModel validate(LoginModel login){

        Optional<UserModel> found = this.repository.findByUsernameAndPass( login.getUsername(),  login.getPassword());
        try {
            return found.get();
        } catch (NoSuchElementException e) {
            System.out.println("No se encontró el elemento");
        }

        return null;
    }

    public boolean delete(Long idToDelete) {

        this.repository.deleteById(idToDelete);

        return true;

    }

    public UserModel edit(UserModel userModel) {

        return this.repository.save(userModel);

    }
}
