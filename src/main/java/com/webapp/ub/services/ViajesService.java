package com.webapp.ub.services;

import com.webapp.ub.model.ViajeModel;
import com.webapp.ub.repositories.IViajeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class ViajesService {
    @Autowired
    IViajeRepository repository;

    public ViajeModel register(ViajeModel newViaje) {
            return this.repository.save(newViaje);
    }

    public List<ViajeModel> getAll(){
        return     repository.findAll();
    }

    public List<ViajeModel> findPassengerTrips(Long id){
        return     repository.findByPasajero(id);
    }

    public ViajeModel findById(Integer id) {

        Optional<ViajeModel> found = this.repository.findById(id);

        try {
            return found.get();
        } catch (NoSuchElementException e) {
            System.out.println("No se encontró el elemento");
        }

        return null;
    }

    public boolean delete(Integer idToDelete) {

        this.repository.deleteById(idToDelete);

        return true;

    }

    public ViajeModel edit(ViajeModel ViajeModel) {

        return this.repository.save(ViajeModel);

    }
}
